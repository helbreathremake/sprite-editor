﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace HelbreathWorld.Common.Assets.Objects
{
    public class NpcHuman : Npc
    {
        public event OwnerHandler StatusChanged;

        private List<Item> equipment;

        private GenderType gender;
        private SkinType skin;
        private int hairStyle;
        private int hairColour;
        private int underwearColour;

        private AdvancedAIBehaviour behaviour;
        private AdvancedAIClass aiClass;
        private AdvancedAIEquipment aiEquipment;
        private AdvancedAIState state;
        private AdvancedAIDifficulty difficulty;

        public NpcHuman(string name)
        {
            IsHuman = true;
            Name = name;
        }

        public void Generate()
        {
            // randomize equipment if Random is specified
            if (this.aiEquipment == AdvancedAIEquipment.Random)
                switch (Dice.Roll(1, 4))
                {
                    case 1: this.aiEquipment = AdvancedAIEquipment.Heavy; break;
                    case 2: this.aiEquipment = AdvancedAIEquipment.Light; break;
                    case 3: this.aiEquipment = AdvancedAIEquipment.Medium; break;
                    default: this.aiEquipment = AdvancedAIEquipment.Civilian; break;
                }

            // set up appearance values
            skin = (SkinType)0; // TODO randomize
            hairColour = 1; // TODO randomize
            hairStyle = 1; // TODO randomize
            underwearColour = 1; // TODO randomize
            gender = (Dice.Roll(1, 2) == 1) ? GenderType.Male : GenderType.Female;

            // inherited
            Type = (gender == GenderType.Male) ? 1 : 4;
            Type += (int)skin;

            Appearance1 = Appearance2 = Appearance3 = Appearance4 = AppearanceColour = 0;
            Appearance1 = Appearance1 | underwearColour;
            Appearance1 = Appearance1 | (hairStyle << 8);
            Appearance1 = Appearance1 | (hairColour << 4);

            GenerateEquipment();
            GenerateStats();
            GenerateAppearance();
        }

        private void GenerateStats()
        {
            // TODO difficulties
            switch (aiClass)
            {
                case AdvancedAIClass.Archer: break; // TODO override npc config for strength etc?
                case AdvancedAIClass.BattleMage: break; // TODO
                case AdvancedAIClass.Mage: break; // TODO override npc config?
                case AdvancedAIClass.Warrior: break; // TODO
            }

            HP = MaxHP;
            MP = MaxMP;

            // update values based on generated equipment
            if (equipment[(int)EquipType.DualHand] != null) AttackRange = equipment[(int)EquipType.DualHand].CriticalRange;
            if (equipment[(int)EquipType.DualHand] != null) BaseDamage = equipment[(int)EquipType.DualHand].DamageSmall;
        }

        private void GenerateEquipment()
        {
            equipment = new List<Item>(16);
            for (int i = 0; i < 15; i++) equipment.Add(null);

            //TODO some sort of festive fun with santa suit?
            //bool isFullSuit = (DateTime.Now.Month == 12 && aiEquipment == AdvancedAIEquipment.Civilian && Dice.Roll(1, 5) > 4) ? true : false;

            // TODO put these items in to config? hard coded and messy
            switch (aiEquipment)
            {
                case AdvancedAIEquipment.Civilian:
                    equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration["Shirt(M)"].Copy() : World.ItemConfiguration["Shirt(W)"].Copy();
                    equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["Tunic(M)"].Copy() : (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration["Bodice(W)"].Copy() : World.ItemConfiguration["LongBodice(W)"].Copy();
                    equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration["Trousers(M)"].Copy() : World.ItemConfiguration["KneeTrousers(M)"].Copy() : (Dice.Roll(1, 2) == 1) ? World.ItemConfiguration["Trousers(W)"].Copy() : World.ItemConfiguration["Skirt(W)"].Copy();
                    switch (Dice.Roll(1,3)) 
                    {
                        case 1: equipment[(int)EquipType.Feet] = World.ItemConfiguration["LongBoots"].Copy(); break;
                        case 2: equipment[(int)EquipType.Feet] = World.ItemConfiguration["Shoes"].Copy(); break;
                        default: break;
                    }

                    // chance to have small shield
                    equipment[(int)EquipType.LeftHand] = (Dice.Roll(1, 3) == 1) ? World.ItemConfiguration["WoodShield"].Copy() : null;

                    // randomize colours of clothes  // TODO - hard coded 15 colours, may change with new client
                    equipment[(int)EquipType.Arms].Colour = Dice.Roll(1, 15);
                    equipment[(int)EquipType.Body].Colour = Dice.Roll(1, 15);
                    equipment[(int)EquipType.Legs].Colour = Dice.Roll(1, 15);
                    if (equipment[(int)EquipType.Feet] != null) equipment[(int)EquipType.Feet].Colour = Dice.Roll(1, 15); 

                    break;
                case AdvancedAIEquipment.Light:
                    equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration["Hauberk(M)"].Copy() : World.ItemConfiguration["Hauberk(W)"].Copy();
                    equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration["ChainHose(M)"].Copy() : World.ItemConfiguration["ChainHose(W)"].Copy();

                    switch (aiClass)
                    {
                        case AdvancedAIClass.Warrior:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["FullHelm(M)"].Copy() : World.ItemConfiguration["FullHelm(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["ScaleMail(M)"].Copy() : World.ItemConfiguration["ScaleMail(W)"].Copy();
                            equipment[(int)EquipType.DualHand] = World.ItemConfiguration["GreatSword"].Copy();
                            break;
                        case AdvancedAIClass.Mage:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["Wizard-Hat(M)"].Copy() : World.ItemConfiguration["Wizard-Hat(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["Robe(M)"].Copy() : World.ItemConfiguration["Robe(W)"].Copy();
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration["MagicWand(MS20)"].Copy();
                            break;
                        case AdvancedAIClass.Archer:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["Wizard-Cap(M)"].Copy() : World.ItemConfiguration["Wizard-Cap(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["LeatherArmor(M)"].Copy() : World.ItemConfiguration["LeatherArmor(W)"].Copy();
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration["ShortBow"].Copy();
                            break;
                        case AdvancedAIClass.BattleMage:
                            //equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["Helm(M)"].Copy() : World.ItemConfiguration["Helm(W)"].Copy();
                            equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["ChainMail(M)"].Copy() : World.ItemConfiguration["ChainMail(W)"].Copy();
                            equipment[(int)EquipType.RightHand] = World.ItemConfiguration["MagicWand(MS20)"].Copy();
                            break;
                    }
                    break;
                case AdvancedAIEquipment.Hero:
                    if (Side == OwnerSide.Aresden)
                    {
                        equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration["aHeroHauberk(M)"].Copy() : World.ItemConfiguration["aHeroHauberk(W)"].Copy();
                        equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration["aHeroLeggings(M)"].Copy() : World.ItemConfiguration["aHeroLeggings(W)"].Copy();

                        switch (aiClass)
                        {
                            case AdvancedAIClass.Warrior:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["aHeroArmor(M)"].Copy() : World.ItemConfiguration["aHeroArmor(W)"].Copy();
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["aHeroHelm(M)"].Copy() : World.ItemConfiguration["aHeroHelm(W)"].Copy();
                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration["GiantSword"].Copy();
                                break;
                            case AdvancedAIClass.Mage:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["aHeroRobe(M)"].Copy() : World.ItemConfiguration["aHeroRobe(W)"].Copy();
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["aHeroCap(M)"].Copy() : World.ItemConfiguration["aHeroCap(W)"].Copy();
                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration["MagicWand(MS20)"].Copy();
                                break;
                        }
                        equipment[(int)EquipType.Back] = World.ItemConfiguration["AresdenHeroCape+1"].Copy();
                    }
                    else
                    {
                        equipment[(int)EquipType.Arms] = (gender == GenderType.Male) ? World.ItemConfiguration["eHeroHauberk(M)"].Copy() : World.ItemConfiguration["eHeroHauberk(W)"].Copy();
                        equipment[(int)EquipType.Legs] = (gender == GenderType.Male) ? World.ItemConfiguration["eHeroLeggings(M)"].Copy() : World.ItemConfiguration["eHeroLeggings(W)"].Copy();

                        switch (aiClass)
                        {
                            case AdvancedAIClass.Warrior:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["eHeroArmor(M)"].Copy() : World.ItemConfiguration["eHeroArmor(W)"].Copy();
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["eHeroHelm(M)"].Copy() : World.ItemConfiguration["eHeroHelm(W)"].Copy();
                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration["GiantSword"].Copy();
                                break;
                            case AdvancedAIClass.Mage:
                                equipment[(int)EquipType.Body] = (gender == GenderType.Male) ? World.ItemConfiguration["eHeroRobe(M)"].Copy() : World.ItemConfiguration["eHeroRobe(W)"].Copy();
                                equipment[(int)EquipType.Head] = (gender == GenderType.Male) ? World.ItemConfiguration["eHeroCap(M)"].Copy() : World.ItemConfiguration["eHeroCap(W)"].Copy();
                                equipment[(int)EquipType.DualHand] = World.ItemConfiguration["MagicWand(MS20)"].Copy();
                                break;
                        }
                        equipment[(int)EquipType.Back] = World.ItemConfiguration["ElvineHeroCape+1"].Copy();
                    }

                    if (equipment[(int)EquipType.DualHand] != null)
                        equipment[(int)EquipType.DualHand].Colour = 5;

                    break;
            }
        }

        private void GenerateAppearance()
        {
            int temp;

            for (int i = 0; i < 15; i++)
                if (equipment[i] != null)
                {
                    Item item = equipment[i];

                    switch (item.EquipType)
                    {
                        case EquipType.RightHand:
                            temp = Appearance2;
                            temp = temp & 0xF00F;
                            temp = temp | (item.Appearance << 4);
                            Appearance2 = temp;

                            temp = AppearanceColour;
                            temp = temp & 0x0FFFFFFF;
                            temp = temp | (item.Colour << 28);
                            AppearanceColour = temp;

                            unchecked
                            {
                                int speed = item.Speed - ((Strength) / 13);
                                if (speed < 0) speed = 0;

                                temp = Status;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | speed;
                                Status = temp;
                            }
                            break;
                        case EquipType.LeftHand:
                            temp = Appearance2;
                            temp = temp & 0xFFF0;
                            temp = temp | (item.Appearance);
                            Appearance2 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xF0FFFFFF;
                                temp = temp | (item.Colour << 24);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.DualHand:
                            temp = Appearance2;
                            temp = temp & 0xF00F;
                            temp = temp | (item.Appearance << 4);
                            Appearance2 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0x0FFFFFFF;
                                temp = temp | (item.Colour << 28);
                                AppearanceColour = temp;
                            }

                            unchecked
                            {
                                int speed = item.Speed - ((Strength) / 13);
                                if (speed < 0) speed = 0;

                                temp = Status;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | speed;
                                Status = temp;
                            }
                            break;
                        case EquipType.Body:
                            if (item.Appearance < 100)
                            {
                                temp = Appearance3;
                                temp = temp & 0x0FFF;
                                temp = temp | (item.Appearance << 12);
                                Appearance3 = temp;
                            }
                            else
                            {
                                temp = Appearance3;
                                temp = temp & 0x0FFF;
                                temp = temp | ((item.Appearance - 100) << 12);
                                Appearance3 = temp;

                                temp = Appearance4;
                                temp = temp | 0x080;
                                Appearance4 = temp;
                            }

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFF0FFFFF;
                                temp = temp | (item.Colour << 20);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Back:
                            temp = Appearance4;
                            temp = temp & 0xF0FF;
                            temp = temp | (item.Appearance << 8);
                            Appearance4 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFF0FFFF;
                                temp = temp | (item.Colour << 16);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Arms:
                            temp = Appearance3;
                            temp = temp & 0xFFF0;
                            temp = temp | (item.Appearance);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFF0FFF;
                                temp = temp | (item.Colour << 12);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Legs:
                            temp = Appearance3;
                            temp = temp & 0xF0FF;
                            temp = temp | (item.Appearance << 8);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFFF0FF;
                                temp = temp | (item.Colour << 8);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Feet:
                            temp = Appearance4;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            Appearance4 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFFFF0F;
                                temp = temp | (item.Colour << 4);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.Head:
                            temp = Appearance3;
                            temp = temp & 0xFF0F;
                            temp = temp | (item.Appearance << 4);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFFFFFF0;
                                temp = temp | (item.Colour);
                                AppearanceColour = temp;
                            }
                            break;
                        case EquipType.FullBody:
                            temp = Appearance3;
                            temp = temp & 0x0FFF;
                            temp = temp | (item.Appearance << 12);
                            Appearance3 = temp;

                            unchecked
                            {
                                temp = AppearanceColour;
                                temp = temp & (int)0xFFF0FFFF;
                                AppearanceColour = temp;
                            }
                            break;
                    }
                }
        }

        public static NpcHuman ParseXml(XmlReader r)
        {
            NpcHuman human = new NpcHuman(r["Name"]);
            human.FriendlyName = (r["FriendlyName"] != null) ? r["FriendlyName"] : human.Name;
            human.Type = (r["Type"] != null) ? Int32.Parse(r["Type"]) : 0;

            OwnerSize ownerSize;
            if (r["Size"] != null && Enum.TryParse<OwnerSize>(r["Size"], out ownerSize))
                human.Size = ownerSize;
            else human.Size = OwnerSize.Small;

            OwnerSide ownerSide;
            if (r["Side"] != null && Enum.TryParse<OwnerSide>(r["Side"], out ownerSide))
                human.Side = ownerSide;
            else human.Side = OwnerSide.Wild;

            human.AttackRange = (r["AttackRange"] != null) ? Int32.Parse(r["AttackRange"]) : 1;
            human.SearchRange = (r["SearchRange"] != null) ? Int32.Parse(r["SearchRange"]) : 5;
            human.BaseDamage = (r["BaseDamageThrow"] != null) ? new Dice(Int32.Parse(r["BaseDamageThrow"]), Int32.Parse(r["BaseDamageRange"]), 0) : new Dice(1, 10, 0);
            human.Dexterity = (r["Dexterity"] != null) ? Int32.Parse(r["Dexterity"]) : 0;
            human.Vitality = (r["Vitality"] != null) ? Int32.Parse(r["Vitality"]) : 0;
            human.Magic = (r["Magic"] != null) ? Int32.Parse(r["Magic"]) : 0;
            human.Spells = (r["Spells"] != null) ? r["Spells"].Split(',') : new string[0];
            human.Experience = (r["BaseExperienceThrow"] != null) ? (new Dice(Int32.Parse(r["BaseExperienceThrow"]), 4, Int32.Parse(r["BaseExperienceThrow"]))) : (new Dice(1, 10, 0));
            human.MaximumDeadTime = (r["MaximumDeadTime"] != null) ? TimeSpan.Parse(r["MaximumDeadTime"]) : new TimeSpan(0, 0, 0, 1);
            human.ActionTime = (r["ActionTime"] != null) ? TimeSpan.Parse(r["ActionTime"]) : new TimeSpan(0, 0, 0, 0, 100);
            human.PhysicalAbsorption = (r["PhysicalAbsorption"] != null) ? Int32.Parse(r["PhysicalAbsorption"]) : 0;
            human.MagicAbsorption = (r["MagicAbsorption"] != null) ? Int32.Parse(r["MagicAbsorption"]) : 0;
            human.MaximumGold = (r["MaximumGold"] != null) ? Int32.Parse(r["MaximumGold"]) : 0;
            human.IsHuman = (r["IsHuman"] != null && Boolean.Parse(r["IsHuman"]));

            if (r["IsStationary"] != null && Boolean.Parse(r["IsStationary"].ToString()))
            {
                human.CurrentAction = MotionType.Idle;
                human.MoveType = MovementType.None;
            }

            if (r["IsStationaryAttack"] != null && Boolean.Parse(r["IsStationaryAttack"].ToString()))
            {
                human.CurrentAction = MotionType.AttackStationary;
                human.MoveType = MovementType.None;
            }

            AdvancedAIBehaviour behaviour;
            if (r["AIBehaviour"] != null && Enum.TryParse<AdvancedAIBehaviour>(r["AIBehaviour"], out behaviour))
                human.Behaviour = behaviour;

            AdvancedAIClass aiClass;
            if (r["AIClass"] != null && Enum.TryParse<AdvancedAIClass>(r["AIClass"], out aiClass))
                human.Class = aiClass;

            AdvancedAIDifficulty difficulty;
            if (r["AIDifficulty"] != null && Enum.TryParse<AdvancedAIDifficulty>(r["AIDifficulty"], out difficulty))
                human.Difficulty = difficulty;

            AdvancedAIEquipment equipment;
            if (r["AIEquipment"] != null && Enum.TryParse<AdvancedAIEquipment>(r["AIEquipment"], out equipment))
                human.Equipment = equipment;

            return human;
        }

        public NpcHuman Copy()
        {
            NpcHuman npc = new NpcHuman(Name);
            npc.FriendlyName = FriendlyName;
            npc.Type = Type;
            npc.Size = Size;
            npc.Side = Side;
            npc.BaseDamage = BaseDamage;
            npc.AttackRange = AttackRange;
            npc.SearchRange = SearchRange;
            npc.Dexterity = Dexterity;
            npc.Vitality = Vitality;
            npc.Magic = Magic;
            npc.Spells = Spells;
            npc.Experience = Experience;
            npc.RemainingExperience = RemainingExperience;
            npc.HP = MaxHP;
            npc.MP = MaxMP;
            npc.PhysicalAbsorption = PhysicalAbsorption;
            npc.MagicAbsorption = MagicAbsorption;
            npc.MaximumDeadTime = MaximumDeadTime;
            npc.ActionTime = ActionTime.Add(new TimeSpan(0, 0, 0, 0, Dice.Roll(1, 300))); // // randomize action time so they dont look robotic!
            npc.MaximumGold = MaximumGold;
            npc.CurrentAction = CurrentAction;
            npc.MoveType = MoveType;
            npc.Behaviour = Behaviour;
            npc.Class = Class;
            npc.State = State;
            npc.Difficulty = Difficulty;
            npc.Equipment = Equipment;

            return npc;
        }

        public void ToggleCombatMode()
        {
            int appearance = ((this.Appearance2 & 0xF000) >> 12);

            if (!IsCombatMode) this.Appearance2 = (0xF000 | Appearance2);
            else this.Appearance2 = (0x0FFF & Appearance2);

            if (StatusChanged != null) StatusChanged(this);
        }

        public AdvancedAIDifficulty Difficulty { get { return difficulty; } set { difficulty = value; } }
        public AdvancedAIBehaviour Behaviour { get { return behaviour; } set { behaviour = value; } }
        public AdvancedAIClass Class { get { return aiClass; } set { aiClass = value; } }
        public AdvancedAIEquipment Equipment { get { return aiEquipment; } set { aiEquipment = value; } }
        public AdvancedAIState State { get { return state; } set { state = value; } }

        public Boolean IsCombatMode { get { return (this.Appearance2 == (0xF000 | Appearance2)); } }
    }
}
