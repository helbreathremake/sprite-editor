﻿namespace PakEditor
{
    partial class PakEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PakEditor));
            this.loadPakFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.addFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.savePakFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.pAKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportSelectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateFramesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blankSpriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fromIndividualFramesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spriteImportProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monsterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.whiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.grassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dirtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pavementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barracksTileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dungeonTileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pakToSprToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batchLegacyConverterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadLegacyPakFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.loadBatchLegacyPakFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.importLegacyPakFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.exportFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.replaceFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.exportLegacyPakFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.addIndividualFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.FileList = new System.Windows.Forms.ListBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.chkShowFrames = new System.Windows.Forms.CheckBox();
            this.chkAnimate = new System.Windows.Forms.CheckBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlFileViewer = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Left = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Top = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Width = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Height = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PivotX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PivotY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DifX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DifY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.liveAnimatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // loadPakFileDialog
            // 
            this.loadPakFileDialog.DefaultExt = "pak";
            this.loadPakFileDialog.FileName = "*.spr";
            this.loadPakFileDialog.Filter = "Sprite files|*.spr";
            this.loadPakFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.LoadFile);
            // 
            // addFileDialog
            // 
            this.addFileDialog.DefaultExt = "png";
            this.addFileDialog.FileName = "*.png";
            this.addFileDialog.Filter = "All Picture Files |*.gif;*.jpg;*.jpeg;*.jpe;*.jfif;*.png;*.bmp;*.dib;*.wmf;*.art;" +
    "*.ico|All Files (*.*)|*.*";
            this.addFileDialog.Multiselect = true;
            this.addFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.AddFile);
            // 
            // savePakFileDialog
            // 
            this.savePakFileDialog.DefaultExt = "spr";
            this.savePakFileDialog.FileName = "*.spr";
            this.savePakFileDialog.Filter = "Sprite Files|*.spr";
            this.savePakFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.SaveAsFile);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.spriteToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripMenuItem1,
            this.toolStripMenuItem6,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.NewFile);
            // 
            // openStripMenuItem
            // 
            this.openStripMenuItem.Name = "openStripMenuItem";
            this.openStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.openStripMenuItem.Text = "Open...";
            this.openStripMenuItem.Click += new System.EventHandler(this.LoadPakFileDialog);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveFile);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.SavePakFileDialog);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(120, 6);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItem1.Text = "Import";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(119, 22);
            this.toolStripMenuItem2.Text = ".PAK File";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.ImportPakFileDialog);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pAKToolStripMenuItem});
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(123, 22);
            this.toolStripMenuItem6.Text = "Export";
            this.toolStripMenuItem6.Visible = false;
            // 
            // pAKToolStripMenuItem
            // 
            this.pAKToolStripMenuItem.Name = "pAKToolStripMenuItem";
            this.pAKToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.pAKToolStripMenuItem.Text = ".PAK File";
            this.pAKToolStripMenuItem.Click += new System.EventHandler(this.ExportToPakDialog);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(120, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.Exit);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem4,
            this.exportToolStripMenuItem,
            this.generateFramesToolStripMenuItem});
            this.editToolStripMenuItem.Enabled = false;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.replaceSelectedToolStripMenuItem,
            this.replaceAllToolStripMenuItem});
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(183, 22);
            this.toolStripMenuItem4.Text = "Replace";
            // 
            // replaceSelectedToolStripMenuItem
            // 
            this.replaceSelectedToolStripMenuItem.Enabled = false;
            this.replaceSelectedToolStripMenuItem.Name = "replaceSelectedToolStripMenuItem";
            this.replaceSelectedToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.replaceSelectedToolStripMenuItem.Text = "Replace Selected...";
            this.replaceSelectedToolStripMenuItem.Click += new System.EventHandler(this.ReplaceSelectedDialog);
            // 
            // replaceAllToolStripMenuItem
            // 
            this.replaceAllToolStripMenuItem.Enabled = false;
            this.replaceAllToolStripMenuItem.Name = "replaceAllToolStripMenuItem";
            this.replaceAllToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.replaceAllToolStripMenuItem.Text = "Replace All...";
            this.replaceAllToolStripMenuItem.Click += new System.EventHandler(this.ReplaceAllDialog);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportSelectedToolStripMenuItem,
            this.exportAllToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // exportSelectedToolStripMenuItem
            // 
            this.exportSelectedToolStripMenuItem.Enabled = false;
            this.exportSelectedToolStripMenuItem.Name = "exportSelectedToolStripMenuItem";
            this.exportSelectedToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.exportSelectedToolStripMenuItem.Text = "Export Selected...";
            this.exportSelectedToolStripMenuItem.Click += new System.EventHandler(this.ExportSelectedDialog);
            // 
            // exportAllToolStripMenuItem
            // 
            this.exportAllToolStripMenuItem.Enabled = false;
            this.exportAllToolStripMenuItem.Name = "exportAllToolStripMenuItem";
            this.exportAllToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.exportAllToolStripMenuItem.Text = "Export All...";
            this.exportAllToolStripMenuItem.Click += new System.EventHandler(this.ExportAllDialog);
            // 
            // generateFramesToolStripMenuItem
            // 
            this.generateFramesToolStripMenuItem.Name = "generateFramesToolStripMenuItem";
            this.generateFramesToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.generateFramesToolStripMenuItem.Text = "Regenerate Frames...";
            this.generateFramesToolStripMenuItem.Click += new System.EventHandler(this.generateFramesToolStripMenuItem_Click);
            // 
            // spriteToolStripMenuItem
            // 
            this.spriteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aToolStripMenuItem,
            this.spriteImportProfileToolStripMenuItem,
            this.toolStripSeparator6,
            this.toolStripMenuItem7});
            this.spriteToolStripMenuItem.Name = "spriteToolStripMenuItem";
            this.spriteToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.spriteToolStripMenuItem.Text = "Sprite";
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blankSpriteToolStripMenuItem,
            this.fromImageToolStripMenuItem,
            this.fromIndividualFramesToolStripMenuItem});
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.aToolStripMenuItem.Text = "Add Sprite";
            // 
            // blankSpriteToolStripMenuItem
            // 
            this.blankSpriteToolStripMenuItem.Name = "blankSpriteToolStripMenuItem";
            this.blankSpriteToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.blankSpriteToolStripMenuItem.Text = "Blank Sprite";
            this.blankSpriteToolStripMenuItem.Click += new System.EventHandler(this.newImageToolStripMenuItem_Click_1);
            // 
            // fromImageToolStripMenuItem
            // 
            this.fromImageToolStripMenuItem.Name = "fromImageToolStripMenuItem";
            this.fromImageToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.fromImageToolStripMenuItem.Text = "From Single Image...";
            this.fromImageToolStripMenuItem.Click += new System.EventHandler(this.AddFileDialog);
            // 
            // fromIndividualFramesToolStripMenuItem
            // 
            this.fromIndividualFramesToolStripMenuItem.Name = "fromIndividualFramesToolStripMenuItem";
            this.fromIndividualFramesToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.fromIndividualFramesToolStripMenuItem.Text = "From Individual Frames...";
            this.fromIndividualFramesToolStripMenuItem.Click += new System.EventHandler(this.fromIndividualFramesToolStripMenuItem_Click);
            // 
            // spriteImportProfileToolStripMenuItem
            // 
            this.spriteImportProfileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monsterToolStripMenuItem});
            this.spriteImportProfileToolStripMenuItem.Name = "spriteImportProfileToolStripMenuItem";
            this.spriteImportProfileToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.spriteImportProfileToolStripMenuItem.Text = "Sprite Import Workflow";
            // 
            // monsterToolStripMenuItem
            // 
            this.monsterToolStripMenuItem.Name = "monsterToolStripMenuItem";
            this.monsterToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.monsterToolStripMenuItem.Text = "Monster (Blender)";
            this.monsterToolStripMenuItem.Click += new System.EventHandler(this.SpriteImportMonster);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(194, 6);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Checked = true;
            this.toolStripMenuItem7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(197, 22);
            this.toolStripMenuItem7.Text = "Crop Frames on Add";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.ToggleCropFrames);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backgroundToolStripMenuItem,
            this.liveAnimatorToolStripMenuItem,
            this.toolStripSeparator5,
            this.toolStripMenuItem3});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // backgroundToolStripMenuItem
            // 
            this.backgroundToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blackToolStripMenuItem,
            this.whiteToolStripMenuItem,
            this.toolStripSeparator3,
            this.grassToolStripMenuItem,
            this.dirtToolStripMenuItem,
            this.pavementToolStripMenuItem,
            this.barracksTileToolStripMenuItem,
            this.dungeonTileToolStripMenuItem});
            this.backgroundToolStripMenuItem.Name = "backgroundToolStripMenuItem";
            this.backgroundToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.backgroundToolStripMenuItem.Text = "Background";
            // 
            // blackToolStripMenuItem
            // 
            this.blackToolStripMenuItem.Name = "blackToolStripMenuItem";
            this.blackToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.blackToolStripMenuItem.Text = "Black";
            this.blackToolStripMenuItem.Click += new System.EventHandler(this.SetBlackBackground);
            // 
            // whiteToolStripMenuItem
            // 
            this.whiteToolStripMenuItem.Name = "whiteToolStripMenuItem";
            this.whiteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.whiteToolStripMenuItem.Text = "White";
            this.whiteToolStripMenuItem.Click += new System.EventHandler(this.SetWhiteBackground);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(177, 6);
            // 
            // grassToolStripMenuItem
            // 
            this.grassToolStripMenuItem.Name = "grassToolStripMenuItem";
            this.grassToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.grassToolStripMenuItem.Text = "Grass Tile";
            this.grassToolStripMenuItem.Click += new System.EventHandler(this.SetGrassBackground);
            // 
            // dirtToolStripMenuItem
            // 
            this.dirtToolStripMenuItem.Name = "dirtToolStripMenuItem";
            this.dirtToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dirtToolStripMenuItem.Text = "Dirt Tile";
            this.dirtToolStripMenuItem.Click += new System.EventHandler(this.SetDirtBackground);
            // 
            // pavementToolStripMenuItem
            // 
            this.pavementToolStripMenuItem.Name = "pavementToolStripMenuItem";
            this.pavementToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pavementToolStripMenuItem.Text = "Pavement Tile";
            this.pavementToolStripMenuItem.Click += new System.EventHandler(this.SetPavementBackground);
            // 
            // barracksTileToolStripMenuItem
            // 
            this.barracksTileToolStripMenuItem.Name = "barracksTileToolStripMenuItem";
            this.barracksTileToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.barracksTileToolStripMenuItem.Text = "Barracks Tile";
            this.barracksTileToolStripMenuItem.Click += new System.EventHandler(this.SetBarracksBackground);
            // 
            // dungeonTileToolStripMenuItem
            // 
            this.dungeonTileToolStripMenuItem.Name = "dungeonTileToolStripMenuItem";
            this.dungeonTileToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.dungeonTileToolStripMenuItem.Text = "Dungeon Tile";
            this.dungeonTileToolStripMenuItem.Click += new System.EventHandler(this.SetDungeonBackground);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Checked = true;
            this.toolStripMenuItem3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItem3.Text = "Lock Frame X Axis";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.ToggleLockFrameYAxis);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pakToSprToolStripMenuItem,
            this.batchLegacyConverterToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // pakToSprToolStripMenuItem
            // 
            this.pakToSprToolStripMenuItem.Name = "pakToSprToolStripMenuItem";
            this.pakToSprToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.pakToSprToolStripMenuItem.Text = "Legacy .PAK Viewer...";
            this.pakToSprToolStripMenuItem.Click += new System.EventHandler(this.PakToSpr);
            // 
            // batchLegacyConverterToolStripMenuItem
            // 
            this.batchLegacyConverterToolStripMenuItem.Name = "batchLegacyConverterToolStripMenuItem";
            this.batchLegacyConverterToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.batchLegacyConverterToolStripMenuItem.Text = "Batch .PAK Converter...";
            this.batchLegacyConverterToolStripMenuItem.Click += new System.EventHandler(this.BatchLegacyConverter);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeToolStripMenuItem,
            this.toolStripMenuItem5,
            this.toolStripSeparator2,
            this.addToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(147, 76);
            // 
            // removeToolStripMenuItem
            // 
            this.removeToolStripMenuItem.Enabled = false;
            this.removeToolStripMenuItem.Name = "removeToolStripMenuItem";
            this.removeToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.removeToolStripMenuItem.Text = "Remove";
            this.removeToolStripMenuItem.Click += new System.EventHandler(this.RemoveFile);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Enabled = false;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(146, 22);
            this.toolStripMenuItem5.Text = "Replace";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.ReplaceSelectedDialog);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(143, 6);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Enabled = false;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.addToolStripMenuItem.Text = "Add Images...";
            this.addToolStripMenuItem.Click += new System.EventHandler(this.AddFileDialog);
            // 
            // loadLegacyPakFileDialog
            // 
            this.loadLegacyPakFileDialog.DefaultExt = "pak";
            this.loadLegacyPakFileDialog.FileName = "*.pak";
            this.loadLegacyPakFileDialog.Filter = "Pak files|*.pak";
            this.loadLegacyPakFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.LoadLegacyPakFile);
            // 
            // loadBatchLegacyPakFileDialog
            // 
            this.loadBatchLegacyPakFileDialog.Multiselect = true;
            this.loadBatchLegacyPakFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ConvertBatchLegacyPakFiles);
            // 
            // importLegacyPakFileDialog
            // 
            this.importLegacyPakFileDialog.DefaultExt = "pak";
            this.importLegacyPakFileDialog.FileName = "*.pak";
            this.importLegacyPakFileDialog.Filter = "Pak files|*.pak";
            this.importLegacyPakFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ImportPakFile);
            // 
            // exportFileDialog
            // 
            this.exportFileDialog.DefaultExt = "png";
            this.exportFileDialog.FileName = "*.png";
            this.exportFileDialog.Filter = "All Picture Files |*.gif;*.jpg;*.jpeg;*.jpe;*.jfif;*.png;*.bmp;*.dib;*.wmf;*.art;" +
    "*.ico|All Files (*.*)|*.*";
            this.exportFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ExportFile);
            // 
            // replaceFileDialog
            // 
            this.replaceFileDialog.DefaultExt = "png";
            this.replaceFileDialog.FileName = "*.png";
            this.replaceFileDialog.Filter = "All Picture Files |*.gif;*.jpg;*.jpeg;*.jpe;*.jfif;*.png;*.bmp;*.dib;*.wmf;*.art;" +
    "*.ico|All Files (*.*)|*.*";
            this.replaceFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ReplaceFile);
            // 
            // exportLegacyPakFileDialog
            // 
            this.exportLegacyPakFileDialog.DefaultExt = "pak";
            this.exportLegacyPakFileDialog.FileName = "*.pak";
            this.exportLegacyPakFileDialog.Filter = "Pak files|*.pak";
            this.exportLegacyPakFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ExportToPak);
            // 
            // addIndividualFileDialog
            // 
            this.addIndividualFileDialog.DefaultExt = "png";
            this.addIndividualFileDialog.FileName = "*.png";
            this.addIndividualFileDialog.Filter = "All Picture Files |*.gif;*.jpg;*.jpeg;*.jpe;*.jfif;*.png;*.bmp;*.dib;*.wmf;*.art;" +
    "*.ico|All Files (*.*)|*.*";
            this.addIndividualFileDialog.Multiselect = true;
            this.addIndividualFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.AddIndividualFrames);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 707);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1008, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(5, 5);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(998, 673);
            this.splitContainer1.SplitterDistance = 190;
            this.splitContainer1.TabIndex = 10;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.FileList);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.flowLayoutPanel1);
            this.splitContainer2.Size = new System.Drawing.Size(190, 673);
            this.splitContainer2.SplitterDistance = 636;
            this.splitContainer2.TabIndex = 15;
            // 
            // FileList
            // 
            this.FileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileList.FormattingEnabled = true;
            this.FileList.Location = new System.Drawing.Point(0, 0);
            this.FileList.Name = "FileList";
            this.FileList.Size = new System.Drawing.Size(190, 636);
            this.FileList.TabIndex = 15;
            this.FileList.SelectedIndexChanged += new System.EventHandler(this.ViewFile);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.chkShowFrames);
            this.flowLayoutPanel1.Controls.Add(this.chkAnimate);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(190, 33);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // chkShowFrames
            // 
            this.chkShowFrames.AutoSize = true;
            this.chkShowFrames.Location = new System.Drawing.Point(8, 8);
            this.chkShowFrames.Name = "chkShowFrames";
            this.chkShowFrames.Size = new System.Drawing.Size(90, 17);
            this.chkShowFrames.TabIndex = 18;
            this.chkShowFrames.Text = "Show Frames";
            this.chkShowFrames.UseVisualStyleBackColor = true;
            this.chkShowFrames.Click += new System.EventHandler(this.ShowFrames);
            // 
            // chkAnimate
            // 
            this.chkAnimate.AutoSize = true;
            this.chkAnimate.Location = new System.Drawing.Point(104, 8);
            this.chkAnimate.Name = "chkAnimate";
            this.chkAnimate.Size = new System.Drawing.Size(64, 17);
            this.chkAnimate.TabIndex = 19;
            this.chkAnimate.Text = "Animate";
            this.chkAnimate.UseVisualStyleBackColor = true;
            this.chkAnimate.CheckedChanged += new System.EventHandler(this.chkAnimate_CheckedChanged_1);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer3.IsSplitterFixed = true;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer3.Size = new System.Drawing.Size(804, 673);
            this.splitContainer3.SplitterDistance = 452;
            this.splitContainer3.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.pnlFileViewer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(804, 452);
            this.panel2.TabIndex = 13;
            // 
            // pnlFileViewer
            // 
            this.pnlFileViewer.AutoScroll = true;
            this.pnlFileViewer.AutoSize = true;
            this.pnlFileViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFileViewer.Location = new System.Drawing.Point(0, 0);
            this.pnlFileViewer.Name = "pnlFileViewer";
            this.pnlFileViewer.Size = new System.Drawing.Size(804, 452);
            this.pnlFileViewer.TabIndex = 12;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.Left,
            this.Top,
            this.Width,
            this.Height,
            this.PivotX,
            this.PivotY,
            this.DifX,
            this.DifY});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.Size = new System.Drawing.Size(804, 217);
            this.dataGridView1.TabIndex = 12;
            // 
            // Index
            // 
            this.Index.HeaderText = "Frame";
            this.Index.MaxInputLength = 1;
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            // 
            // Left
            // 
            this.Left.HeaderText = "Left";
            this.Left.Name = "Left";
            // 
            // Top
            // 
            this.Top.HeaderText = "Top";
            this.Top.Name = "Top";
            // 
            // Width
            // 
            this.Width.HeaderText = "Width";
            this.Width.Name = "Width";
            // 
            // Height
            // 
            this.Height.HeaderText = "Height";
            this.Height.Name = "Height";
            // 
            // PivotX
            // 
            this.PivotX.HeaderText = "PivotX";
            this.PivotX.Name = "PivotX";
            // 
            // PivotY
            // 
            this.PivotY.HeaderText = "PivotY";
            this.PivotY.Name = "PivotY";
            // 
            // DifX
            // 
            this.DifX.HeaderText = "DifX";
            this.DifX.Name = "DifX";
            this.DifX.ReadOnly = true;
            // 
            // DifY
            // 
            this.DifY.HeaderText = "DifY";
            this.DifY.Name = "DifY";
            this.DifY.ReadOnly = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(1008, 683);
            this.panel1.TabIndex = 11;
            // 
            // liveAnimatorToolStripMenuItem
            // 
            this.liveAnimatorToolStripMenuItem.Name = "liveAnimatorToolStripMenuItem";
            this.liveAnimatorToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.liveAnimatorToolStripMenuItem.Text = "Live Animator";
            this.liveAnimatorToolStripMenuItem.Click += new System.EventHandler(this.ViewLiveAnimator);
            // 
            // PakEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PakEditor";
            this.Text = "Helbreath Champions Sprite Editor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog loadPakFileDialog;
        private System.Windows.Forms.OpenFileDialog addFileDialog;
        private System.Windows.Forms.SaveFileDialog savePakFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem removeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pakToSprToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog loadLegacyPakFileDialog;
        private System.Windows.Forms.ToolStripMenuItem batchLegacyConverterToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog loadBatchLegacyPakFileDialog;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.OpenFileDialog importLegacyPakFileDialog;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportAllToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog exportFileDialog;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem replaceSelectedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem replaceAllToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog replaceFileDialog;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem pAKToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog exportLegacyPakFileDialog;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem whiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateFramesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem spriteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromIndividualFramesToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog addIndividualFileDialog;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListBox FileList;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox chkShowFrames;
        private System.Windows.Forms.CheckBox chkAnimate;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn Left;
        private System.Windows.Forms.DataGridViewTextBoxColumn Top;
        private System.Windows.Forms.DataGridViewTextBoxColumn Width;
        private System.Windows.Forms.DataGridViewTextBoxColumn Height;
        private System.Windows.Forms.DataGridViewTextBoxColumn PivotX;
        private System.Windows.Forms.DataGridViewTextBoxColumn PivotY;
        private System.Windows.Forms.DataGridViewTextBoxColumn DifX;
        private System.Windows.Forms.DataGridViewTextBoxColumn DifY;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlFileViewer;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem spriteImportProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blankSpriteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monsterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fromImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dirtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pavementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barracksTileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dungeonTileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem liveAnimatorToolStripMenuItem;
    }
}

