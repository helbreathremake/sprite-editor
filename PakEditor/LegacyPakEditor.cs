﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HelbreathWorld.Game;

namespace PakEditor
{
    public partial class LegacyPakEditor : Form
    {
        private LegacyPakFile pak;
        private int currentSprite;
        private Timer animationTimer;
        private int animationFrame;

        public LegacyPakEditor(string fileName)
        {
            InitializeComponent();

            this.BringToFront();

            animationTimer = new Timer();
            animationTimer.Interval = 100;
            animationTimer.Tick += new EventHandler(ShowSprite);
            animationTimer.Start();

            LoadFile(fileName);
        }

        public void LoadFile(string fileName)
        {
            pak = new LegacyPakFile(fileName);

            foreach (LegacySprite sprite in pak.Sprites.Values)
                FileList.Items.Add(sprite.Index);

            currentSprite = -1;
        }

        private void ShowSprite(object sender, EventArgs e)
        {
            if (FileList.SelectedIndex < 0) return;

            if (chkAnimate.Checked)
            {
                animationFrame++;
                if (animationFrame > pak[FileList.SelectedIndex].Frames.Count-1) animationFrame = 0;

                pictureBox1.Image = pak[FileList.SelectedIndex].GetFrame(animationFrame);
            }
            else
            {
                if (chkShowFrames.Checked)
                    pictureBox1.Image = pak[FileList.SelectedIndex].ImageWithFrames;
                else pictureBox1.Image = pak[FileList.SelectedIndex].Image;
            }

            if (FileList.SelectedIndex != currentSprite)
            {
                dataGridView1.Rows.Clear();
                int index = 0;
                foreach (LegacySpriteFrame frame in pak[FileList.SelectedIndex].Frames)
                {
                    dataGridView1.Rows.Add(new string[] { index.ToString(), frame.Left.ToString(), frame.Top.ToString(), frame.Width.ToString(), frame.Height.ToString(), frame.PivotX.ToString(), frame.PivotY.ToString(), (frame.Width + frame.PivotX).ToString(),(frame.Height + frame.PivotY).ToString() });
                    index++;
                }
            }

            currentSprite = FileList.SelectedIndex;
        }
    }
}
