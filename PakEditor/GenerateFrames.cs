﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Game;

namespace PakEditor
{
    public partial class GenerateFrames : Form
    {
        PakEditor parentForm;

        public GenerateFrames(PakEditor parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int frames;
            int width;
            int height;
            int pivotX;
            int pivotY;

            if (Int32.TryParse(textBox1.Text, out frames)
            && Int32.TryParse(textBox2.Text, out width)
            && Int32.TryParse(textBox3.Text, out height)
            && Int32.TryParse(textBox4.Text, out pivotX)
            && Int32.TryParse(textBox5.Text, out pivotY))
            {
                foreach (var file in parentForm.spriteFile.Sprites)
                {
                    file.Frames.Clear();

                    for (int i = 0; i < frames; i++)
                    {
                        SpriteFrame frame = new SpriteFrame()
                        {
                            Width = width,
                            Height = height,
                            PivotX = pivotX,
                            PivotY = pivotY,
                            Top = 0,
                            Left = i * width
                        };

                        file.Frames.Add(frame);
                    }
                }
            }
            else MessageBox.Show("Invalid Settings");

            Close();
        }
    }
}
