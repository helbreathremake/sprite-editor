﻿
namespace PakEditor
{
    partial class WizardMonster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardMonster));
            this.txtRootFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNorthDirection = new System.Windows.Forms.TextBox();
            this.txtNorthEastDirection = new System.Windows.Forms.TextBox();
            this.txtEastDirection = new System.Windows.Forms.TextBox();
            this.txtSouthEastDirection = new System.Windows.Forms.TextBox();
            this.txtSouthDirection = new System.Windows.Forms.TextBox();
            this.txtSouthWestDirection = new System.Windows.Forms.TextBox();
            this.txtWestDirection = new System.Windows.Forms.TextBox();
            this.txtNorthWestDirection = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDieAction = new System.Windows.Forms.TextBox();
            this.txtTakeDamageAction = new System.Windows.Forms.TextBox();
            this.txtAttackAction = new System.Windows.Forms.TextBox();
            this.txtWalkAction = new System.Windows.Forms.TextBox();
            this.txtIdleAction = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.chkCrop = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRootFolder
            // 
            this.txtRootFolder.Location = new System.Drawing.Point(80, 12);
            this.txtRootFolder.Name = "txtRootFolder";
            this.txtRootFolder.Size = new System.Drawing.Size(230, 20);
            this.txtRootFolder.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Root Folder";
            // 
            // txtNorthDirection
            // 
            this.txtNorthDirection.Location = new System.Drawing.Point(68, 29);
            this.txtNorthDirection.Name = "txtNorthDirection";
            this.txtNorthDirection.Size = new System.Drawing.Size(100, 20);
            this.txtNorthDirection.TabIndex = 2;
            this.txtNorthDirection.Text = "north";
            // 
            // txtNorthEastDirection
            // 
            this.txtNorthEastDirection.Location = new System.Drawing.Point(265, 29);
            this.txtNorthEastDirection.Name = "txtNorthEastDirection";
            this.txtNorthEastDirection.Size = new System.Drawing.Size(100, 20);
            this.txtNorthEastDirection.TabIndex = 3;
            this.txtNorthEastDirection.Text = "northeast";
            // 
            // txtEastDirection
            // 
            this.txtEastDirection.Location = new System.Drawing.Point(68, 55);
            this.txtEastDirection.Name = "txtEastDirection";
            this.txtEastDirection.Size = new System.Drawing.Size(100, 20);
            this.txtEastDirection.TabIndex = 4;
            this.txtEastDirection.Text = "east";
            // 
            // txtSouthEastDirection
            // 
            this.txtSouthEastDirection.Location = new System.Drawing.Point(265, 55);
            this.txtSouthEastDirection.Name = "txtSouthEastDirection";
            this.txtSouthEastDirection.Size = new System.Drawing.Size(100, 20);
            this.txtSouthEastDirection.TabIndex = 5;
            this.txtSouthEastDirection.Text = "southeast";
            // 
            // txtSouthDirection
            // 
            this.txtSouthDirection.Location = new System.Drawing.Point(68, 81);
            this.txtSouthDirection.Name = "txtSouthDirection";
            this.txtSouthDirection.Size = new System.Drawing.Size(100, 20);
            this.txtSouthDirection.TabIndex = 6;
            this.txtSouthDirection.Text = "south";
            // 
            // txtSouthWestDirection
            // 
            this.txtSouthWestDirection.Location = new System.Drawing.Point(265, 81);
            this.txtSouthWestDirection.Name = "txtSouthWestDirection";
            this.txtSouthWestDirection.Size = new System.Drawing.Size(100, 20);
            this.txtSouthWestDirection.TabIndex = 7;
            this.txtSouthWestDirection.Text = "southwest";
            // 
            // txtWestDirection
            // 
            this.txtWestDirection.Location = new System.Drawing.Point(68, 107);
            this.txtWestDirection.Name = "txtWestDirection";
            this.txtWestDirection.Size = new System.Drawing.Size(100, 20);
            this.txtWestDirection.TabIndex = 8;
            this.txtWestDirection.Text = "west";
            // 
            // txtNorthWestDirection
            // 
            this.txtNorthWestDirection.Location = new System.Drawing.Point(265, 107);
            this.txtNorthWestDirection.Name = "txtNorthWestDirection";
            this.txtNorthWestDirection.Size = new System.Drawing.Size(100, 20);
            this.txtNorthWestDirection.TabIndex = 9;
            this.txtNorthWestDirection.Text = "northwest";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "N";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(236, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "NE";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "E";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "SE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "S";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(233, 84);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "SW";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(44, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "W";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(232, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "NW";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtNorthDirection);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtNorthEastDirection);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtEastDirection);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtSouthEastDirection);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtSouthDirection);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtSouthWestDirection);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtWestDirection);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNorthWestDirection);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(379, 139);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Direction Sub Folder Names";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtDieAction);
            this.groupBox2.Controls.Add(this.txtTakeDamageAction);
            this.groupBox2.Controls.Add(this.txtAttackAction);
            this.groupBox2.Controls.Add(this.txtWalkAction);
            this.groupBox2.Controls.Add(this.txtIdleAction);
            this.groupBox2.Location = new System.Drawing.Point(12, 183);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(379, 109);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Action Prefixes";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(35, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 9;
            this.label14.Text = "Die";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(21, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 8;
            this.label13.Text = "Attack";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(184, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "Take Damage";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(227, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Walk";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Idle";
            // 
            // txtDieAction
            // 
            this.txtDieAction.Location = new System.Drawing.Point(65, 71);
            this.txtDieAction.Name = "txtDieAction";
            this.txtDieAction.Size = new System.Drawing.Size(100, 20);
            this.txtDieAction.TabIndex = 4;
            this.txtDieAction.Text = "5. Die";
            // 
            // txtTakeDamageAction
            // 
            this.txtTakeDamageAction.Location = new System.Drawing.Point(265, 45);
            this.txtTakeDamageAction.Name = "txtTakeDamageAction";
            this.txtTakeDamageAction.Size = new System.Drawing.Size(100, 20);
            this.txtTakeDamageAction.TabIndex = 3;
            this.txtTakeDamageAction.Text = "4. TakeDamage";
            // 
            // txtAttackAction
            // 
            this.txtAttackAction.Location = new System.Drawing.Point(65, 45);
            this.txtAttackAction.Name = "txtAttackAction";
            this.txtAttackAction.Size = new System.Drawing.Size(100, 20);
            this.txtAttackAction.TabIndex = 2;
            this.txtAttackAction.Text = "3. Attack";
            // 
            // txtWalkAction
            // 
            this.txtWalkAction.Location = new System.Drawing.Point(265, 19);
            this.txtWalkAction.Name = "txtWalkAction";
            this.txtWalkAction.Size = new System.Drawing.Size(100, 20);
            this.txtWalkAction.TabIndex = 1;
            this.txtWalkAction.Text = "2. Walk";
            // 
            // txtIdleAction
            // 
            this.txtIdleAction.Location = new System.Drawing.Point(65, 19);
            this.txtIdleAction.Name = "txtIdleAction";
            this.txtIdleAction.Size = new System.Drawing.Size(100, 20);
            this.txtIdleAction.TabIndex = 0;
            this.txtIdleAction.Text = "1. Idle";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(316, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BrowseRootFolder);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 321);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(379, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Import";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Import);
            // 
            // chkCrop
            // 
            this.chkCrop.AutoSize = true;
            this.chkCrop.Checked = true;
            this.chkCrop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCrop.Location = new System.Drawing.Point(12, 298);
            this.chkCrop.Name = "chkCrop";
            this.chkCrop.Size = new System.Drawing.Size(95, 17);
            this.chkCrop.TabIndex = 22;
            this.chkCrop.Text = "Crop on Import";
            this.chkCrop.UseVisualStyleBackColor = true;
            // 
            // WizardMonster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 354);
            this.Controls.Add(this.chkCrop);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRootFolder);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "WizardMonster";
            this.Text = "Sprite Editor - Import Wizard (Monster)";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRootFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNorthDirection;
        private System.Windows.Forms.TextBox txtNorthEastDirection;
        private System.Windows.Forms.TextBox txtEastDirection;
        private System.Windows.Forms.TextBox txtSouthEastDirection;
        private System.Windows.Forms.TextBox txtSouthDirection;
        private System.Windows.Forms.TextBox txtSouthWestDirection;
        private System.Windows.Forms.TextBox txtWestDirection;
        private System.Windows.Forms.TextBox txtNorthWestDirection;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDieAction;
        private System.Windows.Forms.TextBox txtTakeDamageAction;
        private System.Windows.Forms.TextBox txtAttackAction;
        private System.Windows.Forms.TextBox txtWalkAction;
        private System.Windows.Forms.TextBox txtIdleAction;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chkCrop;
    }
}