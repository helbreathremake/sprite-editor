﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HelbreathWorld.Common;
using HelbreathWorld.Game;

namespace PakEditor
{
    public partial class LiveAnimator : Form
    {
        private SpriteFile spriteFile;
        private int currentSprite;
        private System.Windows.Forms.Timer animationTimer;
        private int animationFrame;

        public LiveAnimator()
        {
            currentSprite = -1;

            InitializeComponent();

            animationTimer = new System.Windows.Forms.Timer();
            animationTimer.Interval = 100;
            animationTimer.Tick += new EventHandler(AnimateSprite);
            animationTimer.Start();
        }

        private void AnimateSprite(object sender, EventArgs e)
        {
            if (currentSprite < 0) return;

            PictureBox spriteBox;

            if (pnlFileViewer.Controls.Count <= 0)
            {
                spriteBox = new PictureBox();
                spriteBox.Width = spriteFile.Sprites[currentSprite].AnimationWidth;
                spriteBox.Height = spriteFile.Sprites[currentSprite].AnimationHeight;
                spriteBox.Dock = DockStyle.None;
                spriteBox.Left = Math.Abs((spriteBox.Width - pnlFileViewer.Width) / 2);
                spriteBox.Top = Math.Abs((spriteBox.Height - pnlFileViewer.Height) / 2);
                spriteBox.BackColor = Color.Transparent;
                pnlFileViewer.Controls.Add(spriteBox);
            }
            else
            {
                spriteBox = (PictureBox)pnlFileViewer.Controls[0];
                spriteBox.Width = spriteFile.Sprites[currentSprite].AnimationWidth;
                spriteBox.Height = spriteFile.Sprites[currentSprite].AnimationHeight;
            }

            // prepares the image object from the byte stream
            if (!spriteFile.Sprites[currentSprite].ImageLoaded)
                spriteFile.Sprites[currentSprite].LoadImage();

            if (spriteFile.Sprites[currentSprite].Frames.Count > 0)
            {
                animationFrame++;
                if (animationFrame > spriteFile.Sprites[currentSprite].Frames.Count - 1) animationFrame = 0;

                spriteBox.Image = spriteFile.Sprites[currentSprite].GetFrame(animationFrame, false);
            }

        }

        public void ViewFile(SpriteFile spriteFile, int sprite)
        {
            this.spriteFile = spriteFile;
            this.currentSprite = sprite;

            pnlFileViewer.Controls.Clear();
        }

        public void SetWhiteBackground()
        {
            pnlFileViewer.BackgroundImage = null;
            pnlFileViewer.BackColor = Color.White;
        }

        public void SetBlackBackground()
        {
            pnlFileViewer.BackgroundImage = null;
            pnlFileViewer.BackColor = Color.Black;
        }
        public void SetGrassBackground()
        {
            pnlFileViewer.BackgroundImage = (Image)new Bitmap(Properties.Resources.grasstile);
        }

        public void SetDirtBackground()
        {
            pnlFileViewer.BackgroundImage = (Image)new Bitmap(Properties.Resources.dirttile);
        }

        public void SetPavementBackground()
        {
            pnlFileViewer.BackgroundImage = (Image)new Bitmap(Properties.Resources.pavetile);
        }

        public void SetBarracksBackground()
        {
            pnlFileViewer.BackgroundImage = (Image)new Bitmap(Properties.Resources.barrtile);
        }

        public void SetDungeonBackground()
        {
            pnlFileViewer.BackgroundImage = (Image)new Bitmap(Properties.Resources.dungtile);
        }
    }
}
