﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using HelbreathWorld;
using HelbreathWorld.Game;

namespace PakEditor
{
    public partial class WizardMonster : Form
    {
        PakEditor parentForm;

        public WizardMonster(PakEditor parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
        }

        private void BrowseRootFolder(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                txtRootFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Import(object sender, EventArgs e)
        {
            // idle sprites
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthEastDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtEastDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthEastDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthWestDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtWestDirection.Text, txtIdleAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthWestDirection.Text, txtIdleAction.Text));

            // walk sprites
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthEastDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtEastDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthEastDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthWestDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtWestDirection.Text, txtWalkAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthWestDirection.Text, txtWalkAction.Text));

            // attack sprites
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthEastDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtEastDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthEastDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthWestDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtWestDirection.Text, txtAttackAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthWestDirection.Text, txtAttackAction.Text));

            // takedamage sprites
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthEastDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtEastDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthEastDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthWestDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtWestDirection.Text, txtTakeDamageAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthWestDirection.Text, txtTakeDamageAction.Text));

            // die sprites
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthEastDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtEastDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthEastDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtSouthWestDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtWestDirection.Text, txtDieAction.Text));
            parentForm.spriteFile.AddSprite(CreateSprite(txtNorthWestDirection.Text, txtDieAction.Text));

            parentForm.ListFiles();
            parentForm.FileChanged();
        }

        private Sprite CreateSprite(string direction, string action)
        {
            List<Bitmap> bitmaps = GenerateBitmaps(direction, action);

            Bitmap mergedImage = ImageHelper.MergeBitmaps(bitmaps);
            Sprite s = new Sprite();
            s.Width = mergedImage.Width;
            s.Height = mergedImage.Height;
            using (MemoryStream stream = new MemoryStream())
            {
                mergedImage.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                s.ImageData = stream.GetBuffer();
            }

            int currentPoint = 0;
            foreach (var frame in bitmaps)
            {
                s.AddFrame(new SpriteFrame()
                {
                    Top = 0,
                    Left = currentPoint,
                    Width = frame.Width,
                    Height = frame.Height,
                    PivotX = -(frame.Width / 2),
                    PivotY = -(frame.Height / 2)
                });

                currentPoint += frame.Width;
            }

            return s;
        }

        private List<Bitmap> GenerateBitmaps(string direction, string action)
        {
            string location = txtRootFolder.Text.Trim('/') + "\\" + direction + "\\temp\\";

            List<Bitmap> bitmaps = new List<Bitmap>();
            foreach (var file in Directory.GetFiles(location))
            {
                if (Path.GetFileName(file).StartsWith(action))
                {
                    Bitmap bitmap = (Bitmap)Image.FromFile(file);

                    if (chkCrop.Checked)
                        bitmap = ImageHelper.CropBitmap(bitmap);

                    bitmaps.Add(bitmap);
                }
            }

            return bitmaps;
        }
    }
}
